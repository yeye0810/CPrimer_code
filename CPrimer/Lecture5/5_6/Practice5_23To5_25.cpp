//
//  Practice5_23To5_25.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/21.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
//#include <stdexcept>

using namespace std;

int main()
{
    int value1;
    int value2;
    
    while (cin >> value1 >> value2)
    {
        try {
            if (value2 == 0)
            {
                throw runtime_error("Don't input 0 as divide value.");
            }
           
                cout << value1/value2 << endl;
        } catch (runtime_error err) {
            cout << err.what() << "Try agin? y/n" << endl;
            char c;
            cin >> c;
            if (c == 'n')
            {
                break;
            }
        }
    }
    
    return 0;
}
