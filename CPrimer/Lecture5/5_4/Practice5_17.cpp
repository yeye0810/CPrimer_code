//
//  Practice5_17.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/21.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> intVec1{0,1,1,2};//列表初始化
    vector<int> intVec2{0,1,1,2,3,5,8};
    vector<int> intVecS;
    vector<int> intVecL;
    
    if (intVec1.size() > intVec2.size())
    {
        intVecS = intVec2;
        intVecL = intVec1;
    }
    else
    {
        intVecS = intVec1;
        intVecL = intVec2;
    }

    decltype(intVecS.size()) i=0;
    for (; i<intVecS.size(); i++)
    {
        if (intVecS[i] != intVecL[i])
        {
            cout << "The short is not pre of long" << endl;
        }
    }
    if (i == intVecS.size())
    {
        cout << "The short is pre of long" << endl;
    }

    
    return 0;
}
