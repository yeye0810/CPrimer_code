//
//  Practice5_19.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/21.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int main()
{
    string input1;
    string input2;
    string con;
    do{
        cout << "Please input two strings: " << endl;
        cin >> input1 >> input2;//不带空格的string，如果要输入带空格的用getline(cin,input)
        cout << "The short string is ";
        if (input1.size() < input2.size())
        {
            cout << input1 << endl;
        }
        else
        {
            cout << input2 << endl;
        }//cout << (input1.size()<input2.size()?input1:input2) << endl;可以用判断语句输出，此处比用if方便
        cout << "Continue? y/n" << endl;
        cin >> con;
    }while ( con != "n" );
}
