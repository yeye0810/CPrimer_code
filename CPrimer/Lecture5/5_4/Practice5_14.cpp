//
//  Practice5_14.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/21.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    string oneStr;
    vector<string> strVec;
    
    while (cin >> oneStr)
    {
        strVec.push_back(oneStr);
    }
    
    auto curPoint = strVec.begin();
    unsigned maxItem = 0;
    string befStr;
    string maxStr;
    unsigned curItem = 1;
    
    while (curPoint != strVec.end())
    {
        if (*curPoint == befStr)
        {
            ++curItem;
        }
        else
        {
            if (curItem > maxItem)
            {
                maxItem = curItem;
                maxStr = befStr;
            }
            curItem = 1;
        }
        befStr = *curPoint;
        ++curPoint;
    }
    if (curItem > maxItem)
    {
        maxItem = curItem;
        maxStr = *(strVec.end()-1);
    }
    
    cout << "The max string is: " << maxStr << endl;
    cout << "The max item is: " << maxItem << endl;
    
    return 0;
}
