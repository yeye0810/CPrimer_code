//
//  Practice5_5.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/20.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int grade;
    vector<string> letterList = {"A++", "A", "B", "C", "D" , "F"};
    
    while (cin >> grade)
    {
        cout << "Your grade is: ";
        if (grade < 60)
        {
            cout << letterList[5] << endl;
        }
        else if (grade == 100)
        {
            cout << letterList[0] << endl;
        }
        else
        {
            string letterGrade;
            letterGrade=letterList[5-(grade-50)/10];
            
            if (grade % 10 < 3)
            {
                letterGrade += '-';
            }
            else if (grade % 10 >7)
            {
                letterGrade += '+';
            }
            cout << letterGrade << endl;
        }
    }
}
