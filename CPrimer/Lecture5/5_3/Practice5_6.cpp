//
//  Practice5_6.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/20.
//  Copyright © 2018年 姜策. All rights reserved.
//
#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<string> scores = { "F", "D", "C", "B", "A", "A++" };
    
    int grade = 0;
    while (cin >> grade)
    {
        string lettergrade = grade < 60 ? scores[0] : scores[(grade - 50) / 10];
        lettergrade += (grade == 100 || grade < 60) ? "" : (grade % 10 > 7) ? "+" : (grade % 10 < 3) ? "-" : "";
        cout << lettergrade << endl;
    }
    
    
    return 0;
}
