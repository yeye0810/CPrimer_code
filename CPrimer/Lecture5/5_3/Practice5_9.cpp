//
//  Practice5_9.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/20.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    char letter;
    unsigned acout = 0;
    unsigned ecout = 0;
    unsigned icout = 0;
    unsigned ocout = 0;
    unsigned ucout = 0;
    while (cin >> letter)
    {
        if (letter == 'a')
        {
            ++acout;
        }
        else if (letter == 'e')
        {
            ++ecout;
        }
        else if (letter == 'i')
        {
            ++icout;
        }
        else if (letter == 'o')
        {
            ++ocout;
        }
        else if (letter == 'u')
        {
            ++ucout;
        }
    }
}
