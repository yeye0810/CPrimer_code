//
//  Practice5_10And5_11And5_12.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/20.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int main()
{
    char letter;
    unsigned acout = 0;
    unsigned ecout = 0;
    unsigned icout = 0;
    unsigned ucout = 0;
    unsigned ocout = 0;
    unsigned spacecout = 0;
    unsigned tabcout = 0;
    unsigned clinecout = 0;
    char letterbef = '\0';
    unsigned twoLetsCout = 0;
    while (cin >> letter)
    {
        switch (letter) {
            case 'a':
            case 'A':
                ++acout;
                break;
            case 'e':
            case 'E':
                ++ecout;
                break;
            case 'i':
                if (letterbef == 'f')
                {
                    ++twoLetsCout;
                }
            case 'I':
                ++icout;
                break;
            case 'o':
            case 'O':
                ++ocout;
                break;
            case 'u':
            case 'U':
                ++ucout;
                break;
            case ' ':
                ++spacecout;
                break;
            case '\t':
                ++tabcout;
                break;
            case '\n':
                ++clinecout;
                break;
            case 'f':
            case 'l':
                if (letterbef == 'f')
                {
                    ++twoLetsCout;
                }
                break;
            default:
                break;
        }
        letterbef = letter;
    }
}
