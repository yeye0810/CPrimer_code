//
//  Practice7_23.h
//  CPrimer
//
//  Created by 姜策 on 2018/7/7.
//  Copyright © 2018年 姜策. All rights reserved.
//

#ifndef Practice7_23_h
#define Practice7_23_h
#include <stdio.h>
#include <iostream>

using namespace std;

struct Screen
{
public:
    using pos = string::size_type;
    Screen() = default;//默认构造函数
    Screen(pos ht, pos wd):height(ht), width(wd), contents(ht*wd, ' '){}
    Screen(pos ht, pos wd, char c):height(ht), width(wd), contents(ht*wd, c){}
    //Screen(pos ht, pos wd, char c): height(ht), width(wd), contents(ht * wd, c){}
    Screen &move(pos, pos);
    Screen &set(char);

    Screen &display(ostream &os)
    {
        do_display(os);
        return *this;
    }
    /*
    const Screen &display(ostream &os) const
    {
        do_display(os);
        return *this;
    }
     */
    /*
    Screen &dispaly(ostream &os)
    {
        do_display(os);
        return *this;
    }
    const Screen &display(ostream &os) const
    {
        do_display(os);
        return *this;
    }
    */
private:
    pos cursor = 0;
    pos height = 0;
    pos width = 0;
    string contents;
    void do_display(ostream &os) const
    {
        os << contents;
    }
};
Screen &Screen::move(pos ht, pos wd)
{
    cursor = ht * width + wd;
    return *this;
}
Screen &Screen::set(char c)
{
    contents[cursor] =  c;
    return *this;
}
#endif /* Practice7_23_h */
