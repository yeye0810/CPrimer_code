//
//  test.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/7/7.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include "Practice7_32.h"
int main()
{
    Screen myScreen(5, 5, 'X');
    myScreen.move(3, 4);
    myScreen.set('c');
    //myScreen.display(cout);
    Screen myScreen2(6,6,'V');
    Window_mgr scres;
    scres.addItem(myScreen).addItem(myScreen2);
    scres.clear(0);
    //myScreen.display(cout);
    Screen test = scres.returnItem();
    test.display(cout);
    cout << "\n";
}
