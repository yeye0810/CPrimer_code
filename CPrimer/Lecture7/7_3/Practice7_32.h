//
//  Practice7_32.h
//  CPrimer
//
//  Created by 姜策 on 2018/7/8.
//  Copyright © 2018年 姜策. All rights reserved.
//

#ifndef Practice7_32_h
#define Practice7_32_h
#include <iostream>
#include <stdio.h>
#include <vector>

using namespace std;

class Screen;//必须在使用之前先声明

class Window_mgr
{
public:
    using ScreenIndex = std::vector<Screen>::size_type;
    inline void clear(ScreenIndex);
    Window_mgr& addItem(Screen &item)
    {
        screens.push_back(item);
        return *this;
    }
    Screen &returnItem()
    {
        return screens[0];
    }
private:
    vector<Screen> screens;
};

class Screen
{
    friend void Window_mgr::clear(ScreenIndex);
public:
    using pos = string::size_type;
    Screen() = default;
    Screen(pos ht, pos wd): height(ht), width(wd), contents(ht*wd, ' '){}
    Screen(pos ht, pos wd, char c): height(ht), width(wd), contents(wd*ht, c){}
    
    char get() const
    {
        return contents[cursor];
    }
    char get(pos r, pos c) const
    {
        return contents[r*width+c];
    }
    inline Screen &move(pos r, pos c);
    inline Screen &set(char c);
    inline Screen &set(pos r, pos c,char ch);
    
    const Screen& display(ostream &os) const
    {
        do_display(os);
        return *this;
    }
    Screen &display(ostream &os)
    {
        do_display(os);
        return *this;
    }
private:
    void do_display(ostream &os) const
    {
        os << contents;
    }
private:
    pos cursor = 0;
    pos width = 0;
    pos height = 0;
    string contents;
};
inline void Window_mgr::clear(ScreenIndex i)
{
    Screen &s = screens[i];//直接取引用
    s.contents = string(s.height*s.width, ' ');
}
inline Screen &Screen::move(pos r, pos c)
{
    cursor = r*width+c;
    return *this;
}
inline Screen &Screen::set(char c)
{
    contents[cursor] = c;
    return *this;
}
inline Screen &Screen::set(pos r, pos c, char ch)
{
    contents[r*width + c] = ch;
    return *this;
}
#endif /* Practice7_32_h */
