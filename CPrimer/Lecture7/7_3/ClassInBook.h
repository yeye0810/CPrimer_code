//
//  ClassInBook.h
//  CPrimer
//
//  Created by 姜策 on 2018/7/7.
//  Copyright © 2018年 姜策. All rights reserved.
//

#ifndef ClassInBook_h
#define ClassInBook_h
#include <stdio.h>
#include <iostream>
using namespace std;
class Screen{
public:
    typedef string::size_type pos;
    //using pos = string::size_type;
    friend class Window_mgr;
    Screen() = default;
    Screen(pos ht, pos wd, char c): height(ht), width(wd), contents(ht * wd, c){}//赋给content ht*wd个c
    char get() const
    {
        return contents[cursor];
    }
    inline char get(pos ht, pos wd) const;
    Screen &move(pos r, pos c);
    Screen &set(char);
    Screen &set(pos, pos, char);
    Screen &dispaly(ostream &os)
    {
        do_display(os);
        return *this;
    }
    const Screen &display(ostream &os) const
    {
        do_display(os);
        return *this;
    }
private:
    pos cursor = 0;
    pos height = 0;
    pos width = 0;
    string contents;
    void do_display(ostream &os) const
    {
        os << contents;
    }
};
inline
Screen &Screen::move(pos r, pos c)//注意返回的是引用，返回引用是返回数据本身，而返回数值则是返回副本
{
    pos row = r * width;
    cursor = row + c;
    return *this;
}
char Screen::get(pos r, pos c) const
{
    pos row = r * width;
    return contents[row + c];
}
Screen &Screen::set(char c)
{
    contents[cursor] = c;
    return *this;
}
Screen &Screen::set(pos r, pos col, char c)
{
    contents[r*width + col] = c;
    return *this;
}
class Window_mgr{
public:
    using ScreenIndex = vector<Screen>::size_type;
    void clear(ScreenIndex);
private:
    vector<Screen> screens{Screen(24, 80, ' ')};
};
void Window_mgr::clear(ScreenIndex i)
{
    Screen &s = screens[i];
    s.contents = string(s.height * s.width,' ');
}

#endif /* ClassInBook_h */
