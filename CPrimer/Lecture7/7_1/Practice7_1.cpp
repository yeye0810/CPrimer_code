//
//  Practice7_1.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/7/4.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

struct Sales_data
{
    string bookNo;
    unsigned units_sold = 0;
    double revenue = 0.0;
    Sales_data() =default;
    Sales_data(const string &s): bookNo(s){}//分别指参数列表和初始化列表
    Sales_data(const string s, unsigned n, double p): bookNo(s), units_sold(n), revenue(p*n){}
    Sales_data& combine(const Sales_data &rhs)
    {
        units_sold += rhs.units_sold;
        revenue += rhs.revenue;
        return *this;
    }
    string isbn() const
    {
        //不对对象做改变的用const
        return bookNo;
    }
};
istream &read (istream &is, Sales_data &item)
{
    //cin作为输入值
    double price;
    is >> item.bookNo >> item.units_sold >> price;
    item.revenue = price * item.units_sold;
    return is;
}
ostream &print(ostream &os, const Sales_data &item)
{
    os << item.isbn() << " " << item.units_sold << " " <<
    item.revenue ;
    return os;
}
Sales_data add(const Sales_data &lhs, const Sales_data &rhs)
{
    Sales_data sum = lhs;
    sum.combine(rhs);
    return sum;
}
int main()
{
    Sales_data total;
    if (read (cin, total))
    {
        Sales_data trans;
        while (read(cin, trans))
        {
            if (total.isbn() == trans.isbn())
            {
                add(total, trans);
            }
            else
            {
                cout << total.isbn() << " " << total.units_sold << " " << total.revenue << endl;
                total = trans;
            }
        }
        cout << total.isbn() << " " << total.units_sold << " " << total.revenue << endl;
    }
    else
    {
        std::cerr << "No data?!" << std::endl;
        return -1;
    }
    return 0;
}
