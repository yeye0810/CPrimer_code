//
//  test.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/7/7.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include "test.h"
int main()
{
    
    Sales_data item1;
    write(std::cout, item1) << std::endl;
    
    Sales_data item2("0-201-78345-X");//采用构造函数的方式
    write(std::cout, item2) << std::endl;
    
    Sales_data item3("0-201-78345-X", 3, 20.00);
    write(std::cout, item3) << std::endl;
    
    Sales_data item4(std::cin);
    write(std::cout, item4) << std::endl;
    
    return 0;
}
