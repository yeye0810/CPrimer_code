//
//  Practice7_4.hpp
//  CPrimer
//
//  Created by 姜策 on 2018/7/4.
//  Copyright © 2018年 姜策. All rights reserved.
//

#ifndef Practice7_4_hpp
#define Practice7_4_hpp

#include <stdio.h>
#include <iostream>
using namespace std;
struct Person
{
    friend ostream& print(ostream &, const Person);
    friend istream& read(istream &, Person);
private:
    string name;
    string loc;
public:
    Person() = default;
    Person(const string &s1, const string &s2): name(s1), loc(s2) {}
    Person(istream & is){ read(cin, *this);}//this是一个指针
};
ostream& print(ostream& os, Person per)
{
    os << per.name << " " << per.loc;
    return os;
}
istream& read(istream& is,Person per)
{
    is >> per.name >> per.loc;
    return is;
}
#endif /* Practice7_4_hpp */
