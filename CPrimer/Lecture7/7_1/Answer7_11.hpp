//
//  Answer7_11.hpp
//  CPrimer
//
//  Created by 姜策 on 2018/7/4.
//  Copyright © 2018年 姜策. All rights reserved.
//

#ifndef Answer7_11_hpp
#define Answer7_11_hpp

#include <stdio.h>
#include <iostream>
#include <string>//这个要加
using namespace std;//别用这个，每个加std

struct Sales_data;
istream & read( istream &is, Sales_data item);//先声明，那么类就可以使用在后边定义的函数

struct Sales_data
{
    Sales_data() = default;//默认构造函数
    Sales_data(const string &s):bookNo(s){}//使用引用避免赋值
    Sales_data(const string &s,unsigned n, double p):bookNo(s),units_sold(n),revenue(n*p){}
    Sales_data(istream &is)
    {
        read(is, *this);
    }
    string isbn() const{return bookNo;}
    Sales_data& combine(const Sales_data&);
    
    string bookNo;
    unsigned units_sold = 0;
    double revenue = 0.0;
};
/*
std::istream &read(std::istream is, Sales_data &item)
{
    double price = 0;
    is >> item.bookNo >> item.units_sold >> price;
    item.revenue = price * item.units_sold;
    return is;
}
 */
std::istream &read(istream &is, Sales_data &item)
{
    double price = 0;
    is >> item.bookNo >> item.units_sold >> price;
    item.revenue = price * item.units_sold;
    return is;
}
std::ostream &print(ostream &os, const Sales_data &item)
{
    os << item.isbn() << item.units_sold << " " << item.revenue;
    return os;
}
Sales_data add(const Sales_data &lhs, const Sales_data &rhs)
{
    Sales_data sum = lhs;
    sum.combine(rhs);
    return sum;
}

Sales_data& Sales_data::combine(const Sales_data& rhs)
{
    units_sold += rhs.units_sold;
    revenue += rhs.revenue;
    return *this;
}
#endif /* Answer7_11_hpp */
