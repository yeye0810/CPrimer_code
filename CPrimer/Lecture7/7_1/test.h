//
//  test.h
//  CPrimer
//
//  Created by 姜策 on 2018/7/4.
//  Copyright © 2018年 姜策. All rights reserved.
//

#ifndef test_h
#define test_h
#include <string>
#include <iostream>
using namespace std;
struct Sales_data
{
    friend istream &read(istream &, Sales_data &);
    friend ostream &write(ostream &, Sales_data &);
    friend Sales_data add(const Sales_data &, const Sales_data &);
public:
    Sales_data() = default;
    Sales_data(string s): bookNo(s){}
    Sales_data(string s, unsigned n, double p): bookNo(s), units_sold(n), revenue(n*p){}
    Sales_data(istream &is){read(is,*this);}
    string isbn() const{return bookNo;}
    Sales_data& combine(Sales_data const&);
private:
    string bookNo;
    unsigned units_sold = 0;
    double revenue = 0.0;
};
Sales_data& Sales_data::combine(Sales_data const &rhs)
{
    units_sold += rhs.units_sold;
    revenue += rhs.revenue;
    return *this;
}
istream &read(istream &is, Sales_data &item)
{
    double price = 0;
    is >> item.bookNo >> item.units_sold >> price;
    item.revenue = item.units_sold * price;
    return is;
}
ostream &write(ostream &os, Sales_data &item)
{
    os << item.isbn() << item.units_sold << item.revenue;
    return os;
}
Sales_data add(const Sales_data &item1,const Sales_data &item2)
{
    Sales_data sum = item1;
    sum.combine(item2);
    return sum;
}
#endif /* test_h */
