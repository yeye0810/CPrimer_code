//
//  Practice7_53.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/7/10.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>

using namespace std;

class Debug{
    constexpr Debug(bool b = true): hw(b), io(b), other(b) {}
    constexpr Debug(bool h, bool i, bool o): hw(h), io(i), other(o) {}
    constexpr bool any(){return hw || io || other;}
    void set_io(bool b){ io = b;}
    void set_hw(bool b){ hw = b;}
    void set_other(bool b){ other = b;}
private:
    bool hw;
    bool io;
    bool other;
};
