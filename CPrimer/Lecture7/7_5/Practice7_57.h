//
//  Practice7_57.h
//  CPrimer
//
//  Created by 姜策 on 2018/7/10.
//  Copyright © 2018年 姜策. All rights reserved.
//

#ifndef Practice7_57_h
#define Practice7_57_h
class Account {
public:
    void calculate() { amount += amount * interestRate; }
    static double rate() { return interestRate; }
    static void rate(double newRate) { interestRate = newRate; }
    
private:
    std::string owner;
    double amount;
    static double interestRate;
    static constexpr double todayRate = 42.42;
    static double initRate() { return todayRate; }
};

double Account::interestRate = initRate();//静态变量要在类外进行初始化
#endif /* Practice7_57_h */
