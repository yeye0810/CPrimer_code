//
//  Practice7_41.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/7/8.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

struct Sale_data {
    friend istream &read(istream &, Sale_data &);
public:
    Sale_data(string s, unsigned i, double d): bookNo(s), units_sold(i),revenue(i*d)
    {
        cout << "One init" << endl;
    }
    Sale_data(string s):Sale_data(s,0,0){cout << "Two item"<<endl;}
    Sale_data():Sale_data("",0,0){cout << "Three item" << endl;}
    Sale_data(istream &is){
        cout << "Four item" << endl;
        read(is, *this);
    }
    
    
private:
    string bookNo;
    unsigned units_sold;
    double revenue;

};
istream &read(istream &is, Sale_data &item)
{
    double price = 0;
    cin >> item.bookNo >> item.units_sold >> price;
    item.revenue = item.units_sold *price;
    return is;
}
int main()
{
    Sale_data test;
    Sale_data test1("111",1,10);
    Sale_data test2("!!!!");
    Sale_data test3(cin);
    
    return 0;
}
