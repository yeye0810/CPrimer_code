//
//  Practice6_26.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/29.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int main(int argc, char **argv)
{
    try
    {
        if (argc-2 == 2)
        {
            string s;
            s = *argv[1] + *argv[2];
            cout << s << endl;
        }
    }
    catch(runtime_error err)
    {
        cout << err.what() << "Please enter right input" << endl;
    }
}
