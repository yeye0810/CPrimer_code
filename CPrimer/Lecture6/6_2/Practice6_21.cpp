//
//  Practice6_21.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/29.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

void changePoint( int *& p, int *& q)
{
    auto mid = p;
    p = q;
    q = mid;
}

int main()
{
    cout << "Please enter two value: ";
    int value1 , value2;
    cin >> value1 >> value2;
    int *p = &value1;
    int *q = &value2;
    changePoint(p, q);
    cout << "The value is " << *p <<" " << *q << endl;
    return 0;
}
