//
//  Practice6_11.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/28.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

void reset(int &item)
{
    item = 0;
}

int main()
{
    cout << "Please enter your integer: ";
    int item;
    cin >> item;
    reset(item);
    cout << item << endl;
    
    return 0;
}
