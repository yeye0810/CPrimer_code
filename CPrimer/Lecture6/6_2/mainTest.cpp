//
//  mainTest.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/29.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include "Practice6_22.hpp"
int main()
{
    int value1, value2;
    cout << "Please enter two value: ";
    cin >> value1 >> value2;
    int *value2P = &value2;
    cout << "The bigger value is " << compareTwo(value1, value2P) << endl;
    
}
