//
//  Practice6_22.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/29.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include "Practice6_22.hpp"

int compareTwo(const int value1, const int *value2)
{
    if (value1 > *value2)
    {
        return value1;
    }
    else
    {
        return *value2;
    }
}
/*
int main()
{
    int value1, value2;
    cout << "Please enter two value: ";
    cin >> value1 >> value2;
    int *value2P = &value2;
    cout << "The bigger value is " << compareTwo(value1, value2P) << endl;
    
}
 */
 
