//
//  Practice6_27.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/30.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int computeSum(initializer_list<int> ls)
{
    int sum_res = 0;
    for (auto i=ls.begin(); i<ls.end(); i++)
    {
        sum_res += *i;
    }
    return sum_res;
}

int main()
{
    int out = computeSum({1,2,3,4});
    cout << out << endl;
}
