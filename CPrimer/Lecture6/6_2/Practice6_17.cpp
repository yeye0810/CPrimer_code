//
//  Practice6_17.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/29.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include "Practice6_17.hpp"
bool isUpper(const string &value)
{
    decltype(value.size()) length = value.size();
    for (decltype(value.size()) i=0; i<length; i++)
    {
        if (isupper(value[i]))
        {
            return true;
        }
    }
    return false;
}
void changedown(string &value)
{
    auto length = value.size();
    for (decltype(value.size()) i=0; i<length; i++)
    {
        value[i] = tolower(value[i]);
    }
}
int main()
{
    string value = "Hello World";
    cout << isUpper(value) << endl;
    changedown(value);
    cout << value << endl;
    return 0;
}
