//
//  Practice6_10.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/28.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

void changeItem(int *p, int *q)
{
    int midd;
    midd = *p;
    *p = *q;
    *q = midd;
}

int main()
{
    int m;
    int n;
    cout << "Please enter two integer: ";
    cin >> m >> n;
    cout << "The m is " << m << ";The n is " << n << endl;
    changeItem(&m, &n);
    cout << "The m is " << m << ";The n is " << n << endl;
    
    return 0;
    
}
