//
//  Practice6_12.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/28.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

void changeItem(int &p, int &q)
{
    int mid = q;
    q = p;
    p = mid;
}

int main()
{
    int p = 10;
    int q = 1;
    changeItem(p, q);
    cout << p << " "<< q << endl;
    const int ci = 10;
    cout << ci << endl;
}
