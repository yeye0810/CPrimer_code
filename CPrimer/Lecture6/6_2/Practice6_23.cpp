//
//  Practice6_23.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/29.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

void print(int value, const int *p, const int *q)
{
    cout << value << endl;
    for (auto i=p; i<q; i++)
    {
        cout << *i << endl;
    }
}

int main()
{
    int i = 0;
    int j[2] = {0,1};
    print(i, begin(j), end(j));
    
    
}
