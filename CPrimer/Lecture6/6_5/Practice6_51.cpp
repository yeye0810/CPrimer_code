//
//  Practice6_51.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/7/3.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

void f()
{
    cout << "first f" << endl;
}
void f (int)
{
    cout << "second f" << endl;
}
void f(int, int)
{
    cout << "third f" << endl;
}
void f(double, double = 3.14)
{
    cout << "forth f" << endl;
}
int main()
{
    f(42);
    f(42, 0);
    f(2.56, 3.14);
    //f(2.56, 42);
}
