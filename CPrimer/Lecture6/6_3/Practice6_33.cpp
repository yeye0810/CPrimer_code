//
//  Practice6_33.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/7/1.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;
void printVector(vector<int> input, int p)
{
    if (p == input.size())
    {
        return;
    }
    cout << input[p] << endl;
    printVector(input, ++p);
    
}
int main()
{
    vector<int> inputVector = {1, 2, 3, 4};
    printVector(inputVector, 0);
}
