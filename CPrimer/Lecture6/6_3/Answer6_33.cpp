//
//  Answer6_33.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/7/1.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

using Iter = vector<int>::const_iterator;

void print(Iter first, Iter last)
{
    if (first == last)
    {
        cout << "over!" << endl;
        return;
    }
    cout << *first << " ";
    print(++first, last);
    
}

int main()
{
    vector<int> vec{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    print(vec.cbegin(), vec.cend());
    
    return 0;
}
