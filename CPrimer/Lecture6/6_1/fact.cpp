//
//  fact.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/28.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int fact(int num)
{
    int out = 1;
    for (int i=num; i>0; i--)
    {
        out *= i;
    }
    return out;
}

int main()
{
    cout << "Please input a number:";
    int num;
    cin >> num;
    int out = fact(num);
    
    cout << out << endl;
    return 0;
}
