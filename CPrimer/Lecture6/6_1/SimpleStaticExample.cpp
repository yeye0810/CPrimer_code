//
//  SimpleStaticExample.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/28.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

size_t count_calls()
{
    static size_t ctr = 0;
    return ++ctr;
}

int main()
{
    for (int i=0; i<10; i++)
    {
        cout << count_calls() << endl;
    }
    
    return 0;
}
