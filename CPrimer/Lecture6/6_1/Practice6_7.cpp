//
//  Practice6_7.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/28.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int returnAdd()
{
    static unsigned item = 0;
    return ++item;
}

int main()
{
    for (int i=0; i<5; i++)
        cout << returnAdd() << endl;
    return 0;
}
