//
//  6_6.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/28.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int addItem(int input)
{
    static int count = 0;
    return input+(++count);
}
int main()
{
    int input;
    while (cin >> input)
    {
        cout << addItem(input) << endl;
    }
}
