//
//  TypeAlias.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/19.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int main()
{
    int ia[3][4] = {1,2,3,4,5,6,7,8,9,10,11,12};
    using int_array = int[4];//int_array和一个大小为4的整数array相同
    typedef int int_array[4];//int_array[4]是int的同义词
    
    for (int_array *p=ia; p!=ia+3; ++p)
    {
        for (int *q=*p; q!=*p+4; ++q)
        {
            cout << *q << ' ';
        }
        cout << endl;
    }
}
