//
//  Practice3_43.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/19.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int main()
{
    int ia[3][4]={1,2,3,4,5,6,7,8,9,10,11,12};
    //版本1，使用范围迭代工具
    for (const int (&row)[4] : ia)
    {
        for (int col :row)//访问的是每个元素，把每个元素的类型写出来
        {
            cout << col << ' ';
        }
        cout << endl;
    }
    //使用下标运算符
    for (unsigned i=0; i<3; i++)
    {
        for (unsigned j=0; j<4; j++)
        {
            cout << ia[i][j] <<' ';
        }
        cout << endl;
    }
    //使用指针
    for (int (*row)[4]=begin(ia); row<end(ia); row++)
    {
        for (int (*col)=begin(*row); col<end(*row); col++)
        {
            cout << *col << ' ';
        }
        cout << endl;
    }
    //类别名
    using int_array = int[4];
    for (int_array *row=begin(ia); row<end(ia); row++)
    {
        for (int *col=begin(*row); col<end(*row); col++)
        {
            cout << *col << ' ';
        }
        cout << endl;
    }
    return 0;
}

