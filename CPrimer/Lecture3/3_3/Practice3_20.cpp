//
//  Practice3_20.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/17.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int value;
    vector<int> intVec;
    
    while (cin >> value)
    {
        intVec.push_back(value);
    }
    
    if (!intVec.empty())
    {
        
        cout << "Return the neighbor numbers are: " << endl;
        vector<int> sumNeiVec(intVec.size()-1,0);
        for (decltype(intVec.size()) i = 0; i < intVec.size(); i++)
        {
            if (i == 0)
            {
                sumNeiVec[i]=intVec[i];
            }
            else
            {
                sumNeiVec[i] = intVec[i];
                sumNeiVec[i-1] += intVec[i];
            }
        }
        for (auto &c : sumNeiVec)
        {
            cout << c << endl;
        }
    
        cout << "Return the another sum method: " << endl;
        vector<int> sumAnoVec;
        for (decltype(intVec.size()) i=0; i<intVec.size()/2 ; i++)
        {
            int value1 = 0;
            int value2 = 0;
            value1 = intVec[i];
            value2 = intVec[intVec.size()-1-i];
            sumAnoVec.push_back(value1+value2);
        }
        if (intVec.size()%2 != 0)
        {
            decltype(intVec.size()) item = intVec.size()/2;
            sumAnoVec.push_back(intVec[item]);
        }
        for (auto &c : sumAnoVec)
        {
            cout << c << endl;
        }
    }
    
    return 0;
}
