//
//  Practice3_17.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/17.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    string value;
    vector<string> strVec;
    
    cout << "Please Enter Your String: ";
    while (cin >> value)
    {
        strVec.push_back(value);
    }
    
    cout << "The Result: " << endl;
    
    for (auto &oneValue : strVec)
    {
        for (auto &oneChar : oneValue)
        {
            oneChar=toupper(oneChar);//注意toupper只能对单个字符进行操作
        }
        cout << oneValue << endl;
    }
    
    return 0;
}
