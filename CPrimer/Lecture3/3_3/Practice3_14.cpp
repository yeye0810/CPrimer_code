//
//  Practice3_14.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/17.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> vecInt;
    int value;
    
    while(cin >> value)
    {
        vecInt.push_back(value);
    }
    
    for (auto &c : vecInt)//注意不能直接输出vector
    {
        cout << c << endl;
    }
    
    return 0;
    
}
