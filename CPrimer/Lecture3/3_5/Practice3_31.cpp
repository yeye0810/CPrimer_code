//
//  Practice3_31.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/18.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int main()
{
    constexpr int size = 10;
    int a[size];
    int b[size];
    
    for (unsigned i=0; i<size; i++)
    {
        a[i]=i;
    }
    for (unsigned i=0; i<size; i++)
    {
        cout << a[i] << endl;
        b[i]=a[i];
    }
    
    return 0;
}
