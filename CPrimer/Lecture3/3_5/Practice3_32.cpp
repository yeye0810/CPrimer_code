//
//  Practice3_32.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/18.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    unsigned size = 10;
    vector<int> a(size,0);
    unsigned i = 0;
    
    for(auto c=a.begin(); c<a.end(); c++)
    {
        *c = i;
        i++; 
    }
    
    for(auto c=a.begin(); c<a.end(); c++)
    {
        cout << *c << endl;
    }
    return 0;
}
