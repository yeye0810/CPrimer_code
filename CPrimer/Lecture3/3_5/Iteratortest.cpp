//
//  Iteratortest.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/18.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
//#include <iterator>

using namespace std;

int main()
{
    int a[10] = {1,2,3,4,5,6,7,8,9,0};
    int *beg = begin(a);
    int *last = end(a);
    
    for (auto c=beg; c<last; c++)
    {
        cout << *c << endl;
    }
    return 0;
}
