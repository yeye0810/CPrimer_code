//
//  Practice3_41and3_42.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/18.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    //注意sizeof(a)获取的是数组占用的bit大小，而不是数组内数据的个数
    int a[10] = {1,2,3,4,5,6,7,8,9,0};
    vector<int> b(begin(a)+1,end(a)-1);
    unsigned i = 0;
    int bCopyArray[b.size()];
    for(auto &c:b)
    {
        cout << c << endl;
    }
    
    
    for (auto &c:b)
    {
        bCopyArray[i] = c;
        i++;
        cout << "The item is " << i << endl;
    }
    cout << "The size of bCopy "<< sizeof(bCopyArray) << endl;
    for (unsigned i=0; i<b.size(); i++)
    {
        cout << bCopyArray[i] << endl;
    }
    
    return 0;
}
