//
//  Practice3_21.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/18.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<string> strVec;
    string value;
    
    while(cin >> value)
    {
        strVec.push_back(value);
    }
    
    for (auto c=strVec.cbegin(); c!=strVec.cend(); c++)
    {
        cout << *c << " ";
    }
    cout << endl;
    
    return 0;
}
