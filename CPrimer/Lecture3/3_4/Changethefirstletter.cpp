//
//  Changethefirstletter.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/18.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int main()
{
    string s = "hello world";
    for (auto c = s.begin(); c!=s.end() && !isspace(*c); c++)
    {
        *c=toupper(*c);
    }
    cout << s << endl;
    
    return 0;
}
