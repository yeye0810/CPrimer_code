//
//  Practice3_23.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/18.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> intVec;
    int value;
    
    while(cin >> value)
    {
        intVec.push_back(value);
    }
    
    for (auto c=intVec.begin(); c!=intVec.end(); c++)
    {
        *c=*c*2;
        cout << *c << endl;
    }
    
    cout << endl;
    
    return 0;
}
