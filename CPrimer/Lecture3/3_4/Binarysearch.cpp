//
//  Binarysearch.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/18.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> text = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int sought = 11;
    
    auto beg = text.begin();
    auto end = text.end();
    auto mid = text.begin() + (end-beg)/2;
    
    while(mid != end && *mid != sought)
    {
        if (sought < *mid)
        {
            end = mid;
        }
        else
        {
            beg = mid+1;
        }
        mid = beg + (end - beg)/2;
    }
    
    if (mid != end)
    {
        cout << *mid << endl;
    }
    else
    {
        cout << "The value is not in vector" << endl;
    }
}
