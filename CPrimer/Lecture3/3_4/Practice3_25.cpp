/// <summary>
/// Post调用API
/// </summary>
/// <param name="fileToUpload">文件地址</param>
/// <returns>结果集</returns>
public static string HttpPostData(string fileToUpload)
{
    //string uploadUrl = "http://116.62.194.143:8080/PIM_ANPR/singleUpload";
    string uploadUrl = "http://netocr.com/api/recog.do?key=" + ConfigurationManager.AppSettings["OCRKey"] + "&secret=" + ConfigurationManager.AppSettings["OCRSecret"] + "&typeId=19&format=json";
    String fileFormName = "file";
    String contenttype = "image/jpeg";
    string boundary = "----------" + DateTime.Now.Ticks.ToString("x");
    //System.Net.HttpWebRequest
    
    HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(uploadUrl);
    webrequest.ContentType = "multipart/form-data; boundary=" + boundary;
    webrequest.Method = "POST";
    webrequest.AllowWriteStreamBuffering = false;
    
    StringBuilder sb = new StringBuilder();
    sb.Append("--");
    sb.Append(boundary);
    sb.Append("\r\n");
    sb.Append("Content-Disposition: form-data; name=\"");
    sb.Append(fileFormName);
    sb.Append("\"; filename=\"");
    sb.Append(Path.GetFileName(fileToUpload));
    sb.Append("\"");
    sb.Append("\r\n");
    sb.Append("Content-Type: ");
    sb.Append(contenttype);
    sb.Append("\r\n");
    sb.Append("\r\n");
    
    string postHeader = sb.ToString();
    byte[] postHeaderBytes = Encoding.UTF8.GetBytes(postHeader);
    byte[] boundaryBytes = Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
    FileStream fileStream = new FileStream(fileToUpload, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
    long length = postHeaderBytes.Length + fileStream.Length + boundaryBytes.Length;
    webrequest.ContentLength = length;
    
    try
    {
        using (Stream requestStream = webrequest.GetRequestStream())
        {
            requestStream.Write(postHeaderBytes, 0, postHeaderBytes.Length);
            
            byte[] buffer = new Byte[(int)fileStream.Length];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                requestStream.Write(buffer, 0, bytesRead);
            requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
            //requestStream.Close();
            /*using (WebResponse wr = webrequest.GetResponse ()) {
             //wr.GetResponseStream().
             using(StreamReader reader = new StreamReader(wr.GetResponseStream(), Encoding.UTF8)){
             Console.WriteLine (reader.ReadToEnd ());
             }
             }*/
            WebResponse responce = webrequest.GetResponse();
            Stream s = responce.GetResponseStream();
            StreamReader sr = new StreamReader(s);
            fileStream.Close();
            return sr.ReadToEnd();
        }
    }
    catch (Exception ex)
    {
        fileStream.Close();
        logger.Error("Error: [" + ex.Message + "]");
        return null;
    }
    }
