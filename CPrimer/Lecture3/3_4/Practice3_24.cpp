//
//  Practice3_24.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/18.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> intVec = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
    if (intVec.size() % 2 == 0)
    {
        for (auto c=intVec.begin(); c<intVec.end(); c=c+2)
        {
            cout << *c+*(c+1) << endl;
        }
        auto mid = intVec.begin() + (intVec.end() - intVec.begin())/2;
        for (auto c=intVec.begin(); c<mid; c++)
        {
            auto later = intVec.end() - 1 - (c - intVec.begin());
            cout << *c+*later << endl;
        }
    }
    else
    {
        for (auto c=intVec.begin(); c<intVec.end()-1; c=c+2)
        {
            cout << *c+*(c+1) << endl;
        }
        cout << *(intVec.end()-1) << endl;
        auto mid = intVec.begin() + (intVec.end() - intVec.begin())/2;
        for (auto c=intVec.begin(); c<mid; c++)
        {
            auto later = intVec.end() - 1 - (c - intVec.begin());
            cout << *c+*later << endl;
        }
        cout << *mid << endl;
    }
    
    return 0;
}
