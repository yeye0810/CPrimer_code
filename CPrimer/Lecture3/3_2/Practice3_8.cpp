//
//  Practice3_8.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/17.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int main()
{
    string line1;
    string line2;
    
    cout << "Please enter a string: " << endl;
    getline(cin, line1);
    line2=line1;
    
    for (decltype(line1.size()) i = 0; i < line1.size(); i++)
    {
        line1[i]=toupper(line1[i]);
    }
    cout << "The output of for: " << line1 << endl;
    
    decltype(line2.size()) i = 0;
    while (i < line2.size())
    {
        line2[i] = toupper(line2[i]);
        i++;
    }
    cout << "The output of while: " << line2 << endl;
    
    return 0;
}
