//
//  3_2GetLine.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/15.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int main()
{
    string line;
    while (getline(cin,line))
    {
        if(!line.empty())
        {
            unsigned i = 9;
            cout << line << endl;
            cout << line.size()+i << endl;
        }
    }
    
    return 0;
}
