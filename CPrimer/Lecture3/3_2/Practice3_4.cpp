//
//  Practice3_4.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/17.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int main()
{
    string line1;
    string line2;
    
    getline(cin,line1);
    getline(cin,line2);
    //判断是否相等，输出大的那个
    if (line1 != line2)
    {
        if (line1 > line2)
        {
            cout << line1 << endl;
        }
        else
        {
            cout << line2 << endl;
        }
    }
    //判断是否相等，输出长的那个
    if (line1.size() != line2.size())
    {
        if (line1.size() > line2.size())
        {
            cout << line1 << endl;
        }
        else
        {
            cout << line2 << endl;
        }
    }
    
    return 0;
}
