//
//  Stringsubscript.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/17.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int main()
{
    const string hexdigits = "0123456789ABCDEF";
    
    cout << "Enter a series of numbers between 0 and 15" <<
    " separated by soaces. Hit ENTER when finished: "
    <<endl;
    
    string result;
    string::size_type n;
    
    while(cin >> n)
    {
       if (n < hexdigits.size())
       {
           result += hexdigits[n];
       }
    }
    cout << "Your hex nubmber is: " << result << endl;
    
    return 0;
}
