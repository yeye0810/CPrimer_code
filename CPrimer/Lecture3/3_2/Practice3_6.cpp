//
//  Practice3_6.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/17.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int main()
{
    string line;
    cout << "Please enter a string: " << endl;
    
    getline(cin, line);
    for (auto &c : line)
    {
        c=toupper(c);
    }
    
    cout << "The upper output is: " << line << endl;
    return 0;
}
