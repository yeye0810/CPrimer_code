//
//  Pratice3_2.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/17.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;
int main()
{
    string line;
    // 一次读入一整行
    getline(cin, line);
    cout << line << endl;
    //一次读入一个词
    string line2;
    cin >> line2;
    cout << line2 << endl;
    return 0;
}
