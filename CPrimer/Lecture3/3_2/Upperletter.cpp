//
//  Upperletter.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/17.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>


using namespace std;

int main()
{
    string s("Hello World!!!");
    
    for (auto &c : s)//通过引用去改变string中的值
    {
        c = toupper(c);
    }
    cout << s << endl;
    return 0;
}
