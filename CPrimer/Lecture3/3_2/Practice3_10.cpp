//
//  Practice3_10.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/17.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

//使用cctype方法进行判断

int main()
{
    string line;

    cout << "Please enter a string: " << endl;
    getline(cin, line);
    cout << "The output is: ";
    
    for (auto &c : line)
    {
        if (!ispunct(c))
        {
            cout << c;//尽量不要在迭代过程中进行删除操作
        }
    }
    
    cout << endl;
    
    return 0;
}
