//
//  Operatestringchar.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/17.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <cctype>

using namespace std;

int main()
{
    string s("Hello World!!!");
    
    decltype(s.size()) punct_cnt = 0;//获取与s.size()相同的数据类型
    for (auto c : s)
    {
        if (ispunct (c))
        {
            ++punct_cnt;
        }
    }
    cout << punct_cnt
    << " punctation characters in " << s << endl;
    
    return 0;
}
