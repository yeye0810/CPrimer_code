//
//  Pratice3_5.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/17.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int main()
{
    string oneWord;
    string line1;//默认初始化为一个空的字符串
    string line2;
    
    while (cin >> oneWord)
    {
        //直接相连
        line1 = line1 + oneWord;
        //加空格
        line2 = line2 + oneWord + " ";
    }
    cout << line1 << endl;
    cout << line2 << endl;
    
    return 0;
}
