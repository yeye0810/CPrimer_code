//
//  while_test.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/14.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

int main()
{
    int sum = 0;
    int val = 0;
    while(val <= 10)
    {
        sum += val;
        ++val;
    }
    
    std::cout << "Sum of 1 to 10 inclusive is " << sum << std::endl;
    return 0;
}
