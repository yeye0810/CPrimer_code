//
//  Sales_data.h
//  CPrimer
//
//  Created by 姜策 on 2018/6/15.
//  Copyright © 2018年 姜策. All rights reserved.
//

#ifndef Sales_data_h
//检验是否已经被定义，如果没有那么进行定义
#define Sales_data_h



#include <string>

struct Sales_data
{
    std::string bookNo;
    unsigned units_sold = 0;
    double revenue = 0.0;
};
#endif /* Sales_data_h */
