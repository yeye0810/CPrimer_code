#include <iostream>

int main()
{
    bool x1 = false;
    int x2 = x1;
    std::cout << x2 << std::endl;
    x1 = true;
    x2 = x1;
    std::cout << x2 << std::endl;
    unsigned i = 10;
    int j = 20;
    std::cout << i-j << std::endl;
    return 0;
}
