//
//  Practice4_22.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/19.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int main()
{
    int value;
    cin >> value;
    
    //使用条件运算符
    cout << ((value>90?"high pass":(value>75?"pass":(value>60?"low pass":"failed")))) << endl;
    //使用if语句
    if (value >90)
    {
        cout << "high pass" << endl;
    }
    else if (value > 75)
    {
        cout << "pass" << endl;
    }
    else if (value >60)
    {
        cout << "low pass" << endl;
    }
    else
    {
        cout << "pass" << endl;
    }
}
