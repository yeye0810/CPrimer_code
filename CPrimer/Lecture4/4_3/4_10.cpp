//
//  4_10.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/19.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>

using namespace std;

int main()
{
    int value;
    while (cin >> value && value != 42)
    {
        cout << value << endl;
    }
    return 0;
}
