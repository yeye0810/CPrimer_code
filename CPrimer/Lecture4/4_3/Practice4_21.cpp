//
//  Practice4_21.cpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/19.
//  Copyright © 2018年 姜策. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <vector>
using namespace std;

int main()
{
    vector<int> intVec = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    
    for (auto &c : intVec)
    {
        if (c % 2 != 0)
        {
            c = c*2;
        }
    }
    
    for (auto &c : intVec)
    {
        cout << c << endl;
    }
    return 0;
}
