#include <iostream>
#include <stdio.h>
#include <vector>
using namespace std;
class Solution {
public:
    bool isMatch(string s, string p) {
        //p要能覆盖s
        if (p.empty()) //如果为空，那么判断另一个是不是空
        {
            return s.empty();
        }
        if (p.size() == 1)
        {
            return (s.size() == 1 && (s[0] == p[0] || p[0] == '.'));//如果是一个，那么可能是相等也可能是'.'
        }
        if (p[1] != '*')//第二个元素不是'*'
        {
            if (s.empty())
            {
                return false;
            }
            return (s[0] == p[0] || p[0] == '.') && isMatch(s.substr(1), p.substr(1));//取后边的str继续进行判断
        }
        while (!s.empty() && (s[0] == p[0] || p[0] == '.'))//即p的第二个元素是'*'
        {
            if (isMatch(s, p.substr(2)))//跳过*判断后边的
            {
                return true;
            }
            s = s.substr(1);
        }
        return isMatch(s, p.substr(2));
    }
};
int main()
{
    Solution test;
    cout << test.isMatch("aab", "ca*b") << endl;
   
}
