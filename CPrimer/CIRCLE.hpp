//
//  CIRCLE.hpp
//  CPrimer
//
//  Created by 姜策 on 2018/6/29.
//  Copyright © 2018年 姜策. All rights reserved.
//

#ifndef CIRCLE_hpp
#define CIRCLE_hpp
class Circle
{
private:
    double r;//半径
public:
    Circle();//构造函数
    Circle(double R);//构造函数
    double Area();//求面积函数
};

#endif /* CIRCLE_hpp */
